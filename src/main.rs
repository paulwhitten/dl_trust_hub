use std::{fs, path::Path};
use itertools::Itertools;
use std::time::Instant;
use isahc::ResponseExt;
use rayon::prelude::*;
use regex::Regex;

fn main() {
    let start = Instant::now();
    let re = Regex::new(r#"/downloads/resource/([^"]|\\")*"#).unwrap();
    let file_data = fs::read_to_string("untitled.html").unwrap();

    match re
        .find_iter(&file_data)
        .unique_by(|n| n.as_str()) // deduplicate
        .par_bridge() // TODO why is this needed?
        .into_par_iter()
        .map(move |m| {
            
            let path = m.as_str().to_owned();
            match isahc::get("https://trust-hub.org".to_owned() + &path) {
                Ok(mut r) => {
                    match r.copy_to_file(
                        "./data/".to_owned()
                            + Path::new(&path).file_name().unwrap().to_str().unwrap(),
                    ) {
                        Ok(_) => {
                            println!("Wrote: {}", path);
                            Ok(0)
                        }
                        Err(f) => {
                            println!("Error writing file: {:?}", f);
                            Err(format!("Error writing File {}", path))
                        }
                    }
                }
                Err(e) => {
                    println!("Error with GET: {:?}", e);
                    Err(format!("Error in http GET for {}", path))
                }
            }
        })
        .collect::<Result<Vec<_>, _>>()
    {
        Ok(v) => {
            println!("Finished downloading {}", v.len());
        }
        Err(e) => {
            println!("Error: {:?}", e);
        }
    }
    println!("Took: {:?}", start.elapsed());
}
