const fs = require('fs');
const path = require('path');
const axios = require('axios');

// our base URL to download stuff
const baseAddress = `https://trust-hub.org/`

let start = process.hrtime();

// matches on any files from the downloads resource folder, excluding double quotes
let re = /\/downloads\/resource\/([^"]|\\")*/g;

// used to deduplicate downloads
let dlMap = new Map();

// load the file scraped from web
// there are some panes and complexity around getting this at runtime
let fileData = fs.readFileSync('untitled.html').toString();

// get and print the matches in the file
let matches = fileData.match(re);
console.log('matches', matches);

let numMatches = matches.length;
let dupes = 0;
let dl = 0;

/**
 * This function gets the files in the passed array
 * one at a time
 * @param {*} files 
 */
function getDataFiles(files) {
    let f = files.shift();
    console.log("\tgetting", f);
    let basename = path.basename(f);

    // ensure we didn't already get the file
    if (dlMap.get(f)===undefined) {
        // get the file as binary data in an array buffer
        axios.get(baseAddress + f, {headers:{}, responseType: 'arraybuffer'})
            .then((res) => {
                console.log("\t" + basename, 'type:', typeof(res.data)); //, res.headers
                // write the data to a file
                fs.writeFile(path.join('./data', basename), res.data, (err) => {
                    if (err) {
                        // log error to console and do not continue
                        console.log("Error writing file:", basename, err);
                        console.log(files.length, "files not downloaded:", files);
                    } else {
                        dl++;
                        console.log("\twrote:", basename);
                        dlMap.set(f, "wrote");
                        // get the next file if there are more
                        if (files.length > 0) {
                            setImmediate(() => {
                                getDataFiles(files);
                            });
                        }
                    }
                });
            })
            .catch((err) => {
                console.log("Error", err);
            });
    } else {
        console.log("Already got", basename);
        dupes++;
        // get the next file if there are more
        if (files.length > 0) {
            setImmediate(() => {
                getDataFiles(files);
            });
        }
    }
}

process.on("exit", () => {
    let end = process.hrtime(start);
    console.log("matched:", numMatches, "downloads:", dl, "duplicates:", dupes);
    console.log("took:", end);
})

getDataFiles(matches);
