# dl_trust_hub

downloads the trust-hub files relevant to chip level trojans

## javascript

```sh
npm i
mkdir data
node index.js
```

## rust

```sh
mkdir data
cargo run
```